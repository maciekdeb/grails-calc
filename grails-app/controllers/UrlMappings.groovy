class UrlMappings {

    static mappings = {
//        "/$controller/$action?/$id?(.$format)?"{
//            constraints {
//                apply constraints here
//            }
//        }

        "/calc/index/$variable" {
            controller = 'calc'
            action = 'index'
        }

        "/calc/home/$value" {
            controller = 'calc'
            action = 'home'
        }

	"/calc/calcSqrt/$value" {
            controller = 'calc'
            action = 'calcSqrt'
        }

	"/calc/calcSquared/$value" {
            controller = 'calc'
            action = 'calcSquared'
        }

	"/calc/calcCube/$value" {
            controller = 'calc'
            action = 'calcCube'
        }

	"/calc/clear" {
            controller = 'calc'
            action = 'clear'
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
