package grails.calc

class CalcController {

    def index(String variable) {
        render Eval.me(variable)
    }

    def home(String value) {
        render (view: "calculator", model: [myValue : value])
    }

	def calculate(){
		render Eval.me(Double.parseDouble(params.value))
	}

	def calcSqrt(){
		render Math.sqrt(Double.parseDouble(params.value))
	}

	def calcSquared(){
		render Double.parseDouble(params.value)*Double.parseDouble(params.value)
	}

	def calcCube(){
		render Double.parseDouble(params.value)*Double.parseDouble(params.value)*Double.parseDouble(params.value)
	}

	def clear() {
        render ""
    }

}
