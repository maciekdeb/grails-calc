<html>
	<head>
		<meta name="layout" content="main"/>
    <g:javascript library="jquery"/>
		<script>
			var wasResultRecently = false;
			var operations = [' + ', ' - ', ' * ', ' / '];

			function appendValue(value) {
				if(value == "."){
					value = prepareDot(value);
				}
				if(wasResultRecently){
					wasResultRecently = false;
					$("#input").val(value);
				} else {
					$("#input").val($("#input").val() + value);
				}
			}
		
			function prepareDot(value) {
				var expression = $("#input").val();
				var lastChar = expression.slice(-1);
				var lastNumber = expression.split(" ").slice(-1)[0];

				if(isInteger(lastNumber)){
					if(lastChar == " " || lastChar == "" || wasResultRecently){
						return "0" + value;
					} else if(lastChar == ".") {
						return "";
					} else {
						return value;
					}
				} else {
					return "";
				}
			}

			function isInteger(expression){
				if(expression.indexOf(".") == -1){
					return true;
				} else return false; 
			}

			function calculate(expression) {
				if(checkEvaluationValidity(expression)){
					var result = eval(expression);
					$("#input").val(result);
					wasResultRecently = true;
					return result;
				}
				return expression;
			}			

			function checkEvaluationValidity(input){
				var lastPart = input.slice(-3);
				if(operations.indexOf(lastPart) >= 0) {
					return false;
				}
				var arrayLength = operations.length;
				for (var i = 0; i < arrayLength; i++) {
					if(input.indexOf(operations[i]) > -1){
						return true;
					}
				}
				return false;
			}

			function availableOperation(previous, current) {
				var last = previous.slice(-3);
				if(operations.indexOf(last) >= 0 || last == "") {
					return "";
				}	else {
					wasResultRecently = false;
					return ' ' + current + ' ';
				}
			}

			function erase(){
				$.ajax("http://localhost:8080/calc/clear")
					.done(function(msg) {
						$("#input").val( msg );
				  })
				  .fail(function() {
						alert("error");
				  })
				  .always(function() {});
			}
			
			function calcSqrt(expression){
				expression = calculate(expression);
				$.ajax("http://localhost:8080/calc/calcSqrt/"+expression)
					.done(function(msg) {
						$("#input").val(msg);
				  })
				.fail(function() {
						console.log("error in sqrt");
				  });
				wasResultRecently = true;
			}

			function calcSquared(expression){
				expression = calculate(expression);
				$.ajax("http://localhost:8080/calc/calcSquared/"+expression)
					.done(function(msg) {
						$("#input").val(msg);
				  })
				.fail(function() {
						console.log("error in sqrt");
				  });
				wasResultRecently = true;
			}

			function calcCube(expression){
				expression = calculate(expression);
				$.ajax("http://localhost:8080/calc/calcCube/"+expression)
					.done(function(msg) {
						$("#input").val(msg);
				  })
				.fail(function() {
						console.log("error in sqrt");
				  });
				wasResultRecently = true;
			}

		</script>
		<style>
			input[type=button] {
    		width: 7em;  height: 2em;
			}
		</style>
</head>
<body>

<form name="Calc">
	<table>
		<tr>
			<td>
			<input type="text" name="input" id="input" Size="48" style="text-align: right" readonly>
			<br>
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" value="c"  OnClick="erase()">
				<input type="button" value="√"  OnClick="calcSqrt(Calc.input.value)">
				<input type="button" value="x²" OnClick="calcSquared(Calc.input.value)">
				<input type="button" value="x³" OnClick="calcCube(Calc.input.value)">
					<br>			
				<input type="button" value="1"  OnClick="appendValue('1')">
				<input type="button" value="2"  OnCLick="appendValue('2')">
				<input type="button" value="3"  OnClick="appendValue('3')">
				<input type="button" value="+"  OnClick="Calc.input.value += availableOperation(Calc.input.value, '+')">
					<br>
				<input type="button" value="4"  OnClick="appendValue('4')">
				<input type="button" value="5"  OnCLick="appendValue('5')">
				<input type="button" value="6"  OnClick="appendValue('6')">
				<input type="button" value="-"  OnClick="Calc.input.value += availableOperation(Calc.input.value, '-')">
					<br>
				<input type="button" value="7"  OnClick="appendValue('7')">
				<input type="button" value="8"  OnCLick="appendValue('8')">
				<input type="button" value="9"  OnClick="appendValue('9')">
				<input type="button" value="x"  OnClick="Calc.input.value += availableOperation(Calc.input.value, '*')">
					<br>
				<input type="button" value="."  OnClick="appendValue('.')">
				<input type="button" value="0"  OnClick="appendValue('0')">
				<input type="button" value="="  OnClick="calculate(Calc.input.value)">
				<input type="button" value="/"  OnClick="Calc.input.value += availableOperation(Calc.input.value, '/')">	
					<br>
			</td>
		</tr>
	</table>
</form>

<body>
</html>
